﻿namespace TaskEighteen
{
    public class Human : Organism, ITypeCloneable<Human>
    {
        public Human(string name) : base("Human", name)
        {
        }

        public override object Clone()
        {
            return new Human(Name);
        }

        public Human TypeClone()
        {
            return new Human(this.Name);
        }

        public Human TypeClone(string name)
        {
            return new Human(name);
        }
    }
}
