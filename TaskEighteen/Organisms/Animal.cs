﻿namespace TaskEighteen
{
    class Animal : Organism, ITypeCloneable<Animal>
    {
        public Animal(string name) : base("Animal", name)
        {
        }

        public override object Clone()
        {
            return new Animal(Name);
        }

        public Animal TypeClone()
        {
            return new Animal(this.Name);
        }

        public Animal TypeClone(string name)
        {
            return new Animal(name);
        }
    }
}
