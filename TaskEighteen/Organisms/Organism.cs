﻿using System;

namespace TaskEighteen
{
    public class Organism : ICloneable
    {
        public string OrganismType { get; set; }
        public string Name { get; set; }

        public Organism(string type, string name)
        {
            OrganismType = type;
            Name = name;
        }
        public virtual object Clone()
        {
            return new Organism(OrganismType, Name);
        }

        public override string ToString()
        {
            return $"Type: {OrganismType}, name: {Name}";
        }
    }
}
