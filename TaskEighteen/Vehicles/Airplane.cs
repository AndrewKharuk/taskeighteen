﻿namespace TaskEighteen
{
    public class Airplane : Vehicle
    {
        public override string VehicleType { get => base.VehicleType; set => base.VehicleType = "Airplane"; }

        public Airplane() { }

        public Airplane(string collor)
        {
            Collor = collor;
            VehicleType = "Airplane";
        }

        public override object Clone()
        {
            return new Airplane() { VehicleType = this.VehicleType, Collor = this.Collor };
        }
    }
}
