﻿namespace TaskEighteen
{
    public class Car : Vehicle
    {
        public override string VehicleType { get => base.VehicleType; set => base.VehicleType = "Car"; } 

        public Car() { }

        public Car(string collor)
        {
            Collor = collor;
            VehicleType = "Car";
        }

        public override object Clone()
        {
            return new Car() { VehicleType = this.VehicleType, Collor = this.Collor };
        }
    }
}
