﻿using System;

namespace TaskEighteen
{
    public class Vehicle : ICloneable, IVehicleCloneable 
    {

        public virtual string VehicleType { get; set; }
        public string Collor { get; set; }

        public Vehicle()
        {
            VehicleType = "Vehicle";
            Collor = "Abstract";
        }

        public Vehicle(string type, string collor)
        {
            VehicleType = type;
            Collor = collor;
        }

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        public override string ToString()
        {
            return $"Vehicle type: {VehicleType}, collor: {Collor}.";
        }

        public T VehicleClone<T>(string collor) where T : Vehicle, new()
        {
            return new T() { VehicleType = typeof(T).Name, Collor = collor };
        }
    }
}
