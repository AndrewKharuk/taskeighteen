﻿namespace TaskEighteen
{
    interface IVehicleCloneable
    {
        T VehicleClone<T>(string collor) where T : Vehicle, new();
    }
}
