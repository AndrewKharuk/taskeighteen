﻿using System;

namespace TaskEighteen
{
    class Program
    {
        static void Main(string[] args)
        {
            Vehicle obj1 = new Vehicle();
            Console.WriteLine(obj1);

            Vehicle obj2 = (Vehicle)obj1.Clone();
            obj2.VehicleType = "Car";
            obj2.Collor = "Yellow";
            Console.WriteLine(obj2);

            Car obj3 = new Car("Green");
            Console.WriteLine(obj3);

            Car obj4 = (Car)obj3.Clone();
            obj4.Collor = "Blue";
            Console.WriteLine(obj4);

            Car obj5 = obj1.VehicleClone<Car>("White");
            Console.WriteLine(obj5);

            Airplane obj6 = obj1.VehicleClone<Airplane>("Black");
            Console.WriteLine(obj6);

            Human org1 = new Human("Bob");
            Console.WriteLine(org1);

            Human org2 = (Human)org1.Clone();
            org2.Name = "Greg";
            Console.WriteLine(org2);

            Human org3 = org2.TypeClone();
            Console.WriteLine(org3);

            Human org4 = org2.TypeClone("Mike");
            Console.WriteLine(org4);

            Console.ReadKey();
        }
    }
}
