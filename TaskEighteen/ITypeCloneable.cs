﻿namespace TaskEighteen
{
    public interface ITypeCloneable<T> where T : class
    {
        T TypeClone();
        T TypeClone(string name);
    }
}
